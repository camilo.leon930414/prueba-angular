import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Cargos } from '../interfaces/cargos';

@Injectable({
  providedIn: 'root'
})
export class CargosService {

  constructor( private http:HttpClient) { }

  public getCargos(){
    const url ="http://127.0.0.1:8000/api/cargos";
    return this.http.get<Cargos[]>(url);
  }


  public save(cargo){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json; charset=utf-8',
        Authorization: 'my-auth-token'
      })
    };

    const url ="http://127.0.0.1:8000/api/crear_cargo";
    
        cargo.impuestos = cargo.salario*0.08;
        cargo.valor_prima = cargo.salario*2;
        cargo.salud = (cargo.salario*2)*0.08;
        cargo.pension = cargo.salario*0.2;
        cargo.cargo = cargo.cargo;
        console.log(cargo)
        return this.http.post(url,cargo,httpOptions);
  }
}
