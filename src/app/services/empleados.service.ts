import { HttpClient, HttpClientModule, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Empleados } from '../interfaces/empleados';


@Injectable({
  providedIn: 'root'
})
export class EmpleadosService {

  constructor(
    private http:HttpClient
    ) { }

  public getEmpleados(){
    const url ="http://127.0.0.1:8000/api/empleados";
    return this.http.get<Empleados[]>(url);
  }
  public save(empleado){
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json; charset=utf-8',
        Authorization: 'my-auth-token'
      })
    };

    const url ="http://127.0.0.1:8000/api/crear_empleados";
    return this.http.post(url,empleado,httpOptions);
  }
}
