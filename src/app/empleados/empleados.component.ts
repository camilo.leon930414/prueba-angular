import { Component, OnInit } from '@angular/core';
import { Empleados } from '../interfaces/empleados';
import { EmpleadosService } from '../services/empleados.service';

@Component({
  selector: 'app-empleados',
  templateUrl: './empleados.component.html',
  styleUrls: ['./empleados.component.css']
})
export class EmpleadosComponent implements OnInit {
  empleados:Empleados[];
  
  constructor(private empleados_service:EmpleadosService) {
  }

  ngOnInit() {
    this.getAllEmpleados();
  }

  private getAllEmpleados() {
    this.empleados_service.getEmpleados().subscribe(data=>{
      this.empleados = data
    })
    
  }
}
