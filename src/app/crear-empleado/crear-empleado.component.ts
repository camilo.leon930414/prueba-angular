import { Component, OnInit } from '@angular/core';
import { Cargos } from '../interfaces/cargos';
import { Empleados } from '../interfaces/empleados';
import { CargosService } from '../services/cargos.service';
import { EmpleadosService } from '../services/empleados.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-crear-empleado',
  templateUrl: './crear-empleado.component.html',
  styleUrls: ['./crear-empleado.component.css']
})
export class CrearEmpleadoComponent implements OnInit {
  cargos:Cargos[];
  empleados:Empleados = {
      nombre : null,
      documento : null,
      telefono : null,
      direccion : null,
      cargo:'Tecnico'
  };

  constructor(private cargos_service:CargosService,private EmpleadosService:EmpleadosService,private _location: Location) { }

  ngOnInit() {
    this.getAllCargos();
  }

  private getAllCargos() {
        this.cargos_service.getCargos().subscribe(data=>{
            this.cargos = data
        });
  }
  crearEmpleado(){
      this.EmpleadosService.save(this.empleados).subscribe(response=>{
    });
      setTimeout(()=>this.backClicked(),3000)
    }
    backClicked() {
        this._location.back();
    }
}
