export interface Cargos{
    id?:number;
    salario:DoubleRange;
    impuestos:DoubleRange;
    salud:DoubleRange;
    pension:DoubleRange;
    valor_prima:DoubleRange;
    cargo:string;
}