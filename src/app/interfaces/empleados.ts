export interface Empleados{
    id?:number;
    nombre:string;
    documento:string;
    telefono:string;
    direccion:string;
    cargo:string;
}