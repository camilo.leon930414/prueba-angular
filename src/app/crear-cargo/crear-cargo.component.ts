import { Component, OnInit } from '@angular/core';
import { Cargos } from '../interfaces/cargos';
import { CargosService } from '../services/cargos.service';
import {Location} from '@angular/common';

@Component({
  selector: 'app-crear-cargo',
  templateUrl: './crear-cargo.component.html',
  styleUrls: ['./crear-cargo.component.css']
})
export class CrearCargoComponent implements OnInit {
    cargos:Cargos = {
        cargo : null,
        salario : null,
        impuestos : null,
        salud : null,
        pension:null,
        valor_prima:null
    };
    constructor(private cargos_service:CargosService,private _location: Location) { }

    ngOnInit() {
    
    }
    crearCargo(){
        this.cargos_service.save(this.cargos).subscribe(response=>{
        });
        setTimeout(()=>this.backClicked(),3000)
    }
    backClicked() {
        this._location.back();
    }
}
