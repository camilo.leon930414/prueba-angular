import { Component, OnInit } from '@angular/core';
import { Cargos } from '../interfaces/cargos';
import { CargosService } from '../services/cargos.service';

@Component({
  selector: 'app-cargos',
  templateUrl: './cargos.component.html',
  styleUrls: ['./cargos.component.css']
})
export class CargosComponent implements OnInit {
  cargos:Cargos[];
  constructor(private cargos_service:CargosService) { }

  ngOnInit() {
    this.getAllCargos();
  }

  private getAllCargos() {
    this.cargos_service.getCargos().subscribe(data=>{
      this.cargos = data
    })
    
  }
}
