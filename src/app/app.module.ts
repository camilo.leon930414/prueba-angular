import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { EmpleadosComponent } from './empleados/empleados.component';
import { CargosComponent } from './cargos/cargos.component';
import {HttpClientModule} from '@angular/common/http';
import { Route, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { CrearEmpleadoComponent } from './crear-empleado/crear-empleado.component';
import { CrearCargoComponent } from './crear-cargo/crear-cargo.component';
import { FormsModule } from '@angular/forms';

const routes: Route[] = [
  {path: '',component:HomeComponent},
  {path: 'home',component:HomeComponent},
  {path: 'empleados',component:EmpleadosComponent},
  {path: 'crear_empleados',component:CrearEmpleadoComponent},
  {path: 'cargos',component:CargosComponent},
  {path: 'crear_cargos',component:CrearCargoComponent},
]

@NgModule({
  declarations: [
    AppComponent,
    EmpleadosComponent,
    CargosComponent,
    HomeComponent,
    CrearEmpleadoComponent,
    CrearCargoComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    RouterModule.forRoot(routes),
    FormsModule
  ],
  exports: [RouterModule],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
